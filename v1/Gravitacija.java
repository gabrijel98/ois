import java.util.*;
import java.math.*;


public class Gravitacija{
    
    
    public static final double C =6.674*Math.pow(10,-11);
    public static final double M=5.972*Math.pow(10,24);
    public static final double R=6.371*Math.pow(10,6);
    
    public static void main (String[] args) {
        //System.out.println("OIS je zakon!");
        
        Scanner sc = new Scanner(System.in);
        
        double h = sc.nextFloat();
        
        double hMetrih = h*1000;
        
        double a = (C*M)/(Math.pow((R+hMetrih),2));
        
        
        // komentar cisto zbog provjere
        
        
        
        
        System.out.printf("Gravitacijski pospešek na %.2f nadmorske višine je %.2f m/s^2\n", h , a);
    }
    
    
    
}